<?php

namespace Drupal\uw_media\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;
use Drupal\media\OEmbed\Resource;
use Drupal\media\OEmbed\ResourceException;
use Drupal\media\Plugin\Field\FieldFormatter\OEmbedFormatter;

/**
 * Plugin implementation of the 'remote_video' formatter.
 *
 * @internal
 *   This is an internal part of the oEmbed system and should only be used by
 *   oEmbed-related code in Drupal core.
 *
 * @FieldFormatter(
 *   id = "remote_video",
 *   label = @Translation("Remote video content"),
 *   field_types = {
 *     "link",
 *     "string",
 *     "string_long",
 *   },
 * )
 */
class RemoteVideoFormatter extends OEmbedFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $max_width = $this->getSetting('max_width');
    $max_height = $this->getSetting('max_height');

    foreach ($items as $delta => $item) {
      $main_property = $item->getFieldDefinition()->getFieldStorageDefinition()->getMainPropertyName();
      $value = $item->{$main_property};

      if (empty($value)) {
        continue;
      }

      try {
        $resource_url = $this->urlResolver->getResourceUrl($value, $max_width, $max_height);
        $resource = $this->resourceFetcher->fetchResource($resource_url);
      }
      catch (ResourceException $exception) {
        $this->logger->error("Could not retrieve the remote URL (@url).", ['@url' => $value]);
        continue;
      }

      if ($resource->getType() === Resource::TYPE_LINK) {
        $element[$delta] = [
          '#title' => $resource->getTitle(),
          '#type' => 'link',
          '#url' => Url::fromUri($value),
        ];
      }
      elseif ($resource->getType() === Resource::TYPE_PHOTO) {
        $element[$delta] = [
          '#theme' => 'image',
          '#uri' => $resource->getUrl()->toString(),
          '#width' => $max_width ?: $resource->getWidth(),
          '#height' => $max_height ?: $resource->getHeight(),
        ];
      }
      else {
        $url = Url::fromRoute('media.oembed_iframe', [], [
          'query' => [
            'url' => $value,
            'max_width' => $max_width,
            'max_height' => $max_height,
            'hash' => $this->iFrameUrlHelper->getHash($value, $max_width, $max_height),
          ],
        ]);

        $domain = $this->config->get('iframe_domain');
        if ($domain) {
          $url->setOption('base_url', $domain);
        }

        // Render videos and rich content in an iframe for security reasons.
        // @see: https://oembed.com/#section3
        $element[$delta] = [
          '#type' => 'html_tag',
          '#tag' => 'iframe',
          '#attributes' => [
            'src' => $url->toString(),
            'aria-label' => $resource->getTitle(),
            'frameborder' => 0,
            'scrolling' => FALSE,
            'allowtransparency' => TRUE,
            'width' => $max_width ?: $resource->getWidth(),
            'height' => $max_height ?: $resource->getHeight(),
            'class' => ['media-oembed-content'],
          ],
          '#attached' => [
            'library' => [
              'media/oembed.formatter',
            ],
          ],
        ];

        // An empty title attribute will disable title inheritance, so only
        // add it if the resource has a title.
        $title = $resource->getTitle();
        if ($title) {
          $element[$delta]['#attributes']['title'] = $title;
        }

        CacheableMetadata::createFromObject($resource)
          ->addCacheTags($this->config->getCacheTags())
          ->applyTo($element[$delta]);
      }
    }
    return $element;
  }

}
